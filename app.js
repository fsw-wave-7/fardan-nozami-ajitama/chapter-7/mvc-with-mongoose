const express = require("express");
// call library mongoose
const mongoose = require("mongoose");

// callmodel User
const User = require("./models/user");

// call library session express
const session = require("express-session");

// save session to mongodb
const MongoDBStore = require("connect-mongodb-session")(session);

const morgan = require("morgan");

// call routing
const authRoutes = require("./routes/auth");
const productRoutes = require("./routes/products");

// library security for csrf attact
const csrf = require("csurf");
const csrfProtection = csrf();

// library for flash message
const flash = require("connect-flash");

const MONGODB_URI = "mongodb://localhost:27017/mvc_mongo";

// app use view engine ejs
const app = express();

// set store session to mongodb and set for csrf feature
const store = new MongoDBStore({
  uri: MONGODB_URI,
  collection: "session",
});

app.set("view engine", "ejs");

// get request from raw json
app.use(express.json());

// get request from urlencoded
app.use(express.urlencoded({ extended: false }));

// get static from folder public
app.use(express.static("public"));
app.use(morgan("dev"))
app.use(
  session({
    secret: "rahasia",
    resave: false,
    saveUninitialized: false,
    store,
  })
);

// use csrf middleware
app.use(csrfProtection);

// use middleware flash message
app.use(flash());

// Middleware check session user id login and set for csrf feature
app.use((req, res, next) => {
  if (!req.session.user) {
    return next();
  }

  User.findById(req.session.user._id)
    .then((user) => {
      req.user = user;
      next();
    })
    .catch((err) => console.log(err));
});

// set middleware for csrf dan session
app.use((req, res, next) => {
  res.locals.diijinkan = req.session.masuk;
  res.locals.csrfToken = req.csrfToken();
  next();
});

app.use(authRoutes);
app.use(productRoutes);

// connection from mongodb database
mongoose
  .connect(MONGODB_URI, {
    useUnifiedTopology: true,
    useNewUrlParser: true,
    useFindAndModify: false,
  })
  .then((result) => {
    app.listen(3000, () => {
      console.log(`Server berhasil dijalankan pada http://localhost:3000/`);
    });
  })
  .catch((err) => {
    console.log(err);
  });

// const Product = require("./models/product.js");
// const db = mongoose.connection;
// db.on("error", console.error.bind("console", "connection error:"));
// db.once("open", async () => {
// const product = new Product();
// product.title = "ayam goreng";
// product.price = 12000;
// product.description = "ayam goreng mantab";
// product.imageUrl = "ayamgoreng.jpg";
// save data
// const saveProduct = await product.save();
// console.log(saveProduct);
// console.log(product);
// update product
// const updateProduct = await Product.updateOne(
//   { _id: "609c120abac687cc3609534b" },
//   {
//     imageUrl: "http://via.placeholder.com/150",
//   }
// );
// console.log(updateProduct);
// delete product
//   const deleteProduct = await Product.deleteOne({
//     _id: "609c120abac687cc3609534b",
//   });
//   console.log(deleteProduct);
// });

const crypto = require("crypto");
const bcrypt = require("bcrypt");
const User = require("../models/user");

exports.getSignup = (req, res, next) => {
  let message = req.flash("error");
  if (message.length > 0) {
    message = message[0];
  } else {
    message = null;
  }
  res.render("auth/signup", { title: "signup", errorMessage: message });
};

exports.postSignup = (req, res, next) => {
  const { email, password } = req.body;
  User.findOne({
    email,
  })
    .then((user) => {
      if (user) {
        req.flash("error", "Email sudah terdaftar");
        return res.redirect("/signup");
      }
      return bcrypt.hash(password, 12).then((encryptPassword) => {
        User.create({
          email,
          password: encryptPassword,
        });
      });
    })
    .then((result) => {
      res.redirect("/login");
    })
    .catch((err) => {
      console.log(err);
    });
};

exports.getLogin = (req, res, next) => {
  let message = req.flash("error");
  if (message.length > 0) {
    message = message[0];
  } else {
    message = null;
  }
  res.render("auth/login", { title: "login", errorMessage: message });
};

exports.postLogin = (req, res, next) => {
  const { email, password } = req.body;

  User.findOne({
    email,
  })
    .then((user) => {
      if (!user) {
        req.flash("error", "Email tidak Terdaftar");
        return res.redirect("/login");
      }

      bcrypt
        .compare(password, user.password)
        .then((cocok) => {
          if (cocok) {
            req.session.masuk = true;
            req.session.user = user;
            return req.session.save((err) => {
              console.log(err);
              res.redirect("/");
            });
          }
          req.flash("error", "Password yang anda masukkan salah");
          res.redirect("/login");
        })
        .catch((err) => {
          console.log(err);
          res.redirect("/login");
        });
    })
    .catch((err) => {
      console.log(err);
      res.redirect("/login");
    });
};

exports.getHome = (req, res, next) => {
  res.render("auth/home", { title: "Homepage" });
};

exports.logout = (req, res, next) => {
  req.session.destroy((err) => {
    console.log(err);
    res.redirect("/login");
  });
};

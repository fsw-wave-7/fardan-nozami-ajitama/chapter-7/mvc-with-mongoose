const Product = require("../models/product");
exports.getProducts = async (req, res, next) => {
  await Product.find().exec((err, products) => {
    res.render("products/index", { title: "All Products", products });
  });
};

module.exports = (req, res, next) => {
  if (!req.session.masuk) {
    return res.redirect("/login");
  }
  next();
};

const express = require("express");
const authController = require("../controllers/auth");
const router = express.Router();
const diijinkan = require("../middleware/auth");

router.get("/login", authController.getLogin);
router.post("/login", authController.postLogin);
router.get("/signup", authController.getSignup);
router.post("/signup", authController.postSignup);
router.use(diijinkan);
router.post("/logout", authController.logout);
router.get("/", authController.getHome);

module.exports = router;

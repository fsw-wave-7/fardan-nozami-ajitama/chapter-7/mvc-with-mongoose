const express = require("express");
// call Controller
const productController = require("../controllers/products");
// call function router from express
const router = express.Router();

router.get("/products", productController.getProducts);

module.exports = router;
